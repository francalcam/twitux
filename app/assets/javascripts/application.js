// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require activestorage
//= require turbolinks
//= require jquery
//= require jquery-ui
//= require bootstrap-sprockets
//= require bootstrap-modal
//= require bootstrap-modalmanager
//= require_tree .

// console.log("ENTRA");

window.onload = function() {

  $('[nav-button]').click(function(){
    localStorage.setItem('tab', this.getAttribute("nav-button") );
  });
  $('[nav-email]').click(function(){
    localStorage.setItem('email', this.getAttribute("nav-email") );
  });
  //Situa en la última Tab del menú
  if(localStorage.getItem('tab')!==undefined){
    $('[nav-button="'+localStorage.getItem('tab')+'"]').click();
  }
  //Situa en la última Tab de los mensajes
  if(localStorage.getItem('email')!==undefined){
    $('[nav-email="'+localStorage.getItem('email')+'"]').click();
  }
};
