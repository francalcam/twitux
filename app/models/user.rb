class User < ApplicationRecord
  acts_as_authentic

  # Validate email, login, and password as you see fit.
  #
  # Authlogic < 5 added these validation for you, making them a little awkward
  # to change. In 4.4.0, those automatic validations were deprecated. See
  # https://github.com/binarylogic/authlogic/blob/master/doc/use_normal_rails_validation.md

  has_many :posts, :dependent => :destroy

  has_many :user_favorite_posts
  has_many :favorites, :through => :user_favorite_posts, source: :post

  has_one :asset, :as => :attachable, :dependent => :destroy
  accepts_nested_attributes_for :asset, :allow_destroy => true

  has_many :messages, :dependent => :destroy

  #has_many :followers
  #has_many :following
  #https://www.railstutorial.org/book/following_users
  #https://stackoverflow.com/questions/2168442/many-to-many-relationship-with-the-same-model-in-rails

  has_many :user_connections, :foreign_key => :follower_id
  has_many :followers, through: :user_connections, :source => :follower
  has_many :following, through: :user_connections, source: :followed

  #scope :following, -> (id) { where(user_id: id) }

  #http://tutoronrails.com/pages/7-relaciones-polimorficas
  validates :login, :presence => true
  validates :login, uniqueness: true
  validates :email,
    format: {
      with: /@/,
      message: "should look like an email address."
    },
    length: { maximum: 100 },
    uniqueness: {
      case_sensitive: false,
      if: :will_save_change_to_email?
    }

  #validates_format_of :login, :with => /^[-a-z]+$/

  validates :login,
    format: {
      with: /\A[a-z0-9ñ]+\z/,
      message: "should use only letters and numbers."
    },
    length: { within: 3..100 },
    uniqueness: {
      case_sensitive: false,
      if: :will_save_change_to_login?
    }

  validates :password,
    confirmation: { if: :require_password? },
    length: {
      minimum: 8,
      if: :require_password?
    }
  validates :password_confirmation,
    length: {
      minimum: 8,
      if: :require_password?
  }

  def full_name
    "#{self.name} #{self.last_name}"
  end


end
