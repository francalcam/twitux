class Post < ApplicationRecord
   has_ancestry

   belongs_to :user

   has_many :post_connections, :foreign_key => :post_a_id
   has_many :posts, :through => :post_connections, :source => :post_b

   has_many :user_favorite_posts
   has_many :favorites, :through => :user_favorite_posts, :source => :user
   accepts_nested_attributes_for :favorites, :allow_destroy => true

   has_many :assets, :as => :attachable, :dependent => :destroy
   accepts_nested_attributes_for :assets, :allow_destroy => true

   validates_length_of :text, :maximum => 140
   validates :text,:user, :presence => true

   #scope :post, -> {where(post_id: id)}
   scope :my_posts, -> (id) { where(user_id: id) }
   scope :followers, -> (ids) { where(user_id: ids) }

   def post
     Post.find_by_id post_id
   end
end
