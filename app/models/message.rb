class Message < ApplicationRecord
  belongs_to :user

  # def send
  # end
   scope :received, -> (id) { where(recipient_id: id) }

  # def self.received(user)
  #   Message.where(recipient_id:user)
  # end
end
