class Search
  include ActiveModel::Model
  attr_accessor :user

  def find(user)
    User.where("name ILIKE ? OR last_name ILIKE ? OR login ILIKE ?","%#{user}%","%#{user}%","%#{user}%")
  end
end
