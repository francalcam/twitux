class Asset < ApplicationRecord
  belongs_to :attachable, polymorphic: true

  has_attached_file :data,
    styles:          { thumb:  '120x120#',            medium: '600x600>'           },
    convert_options: { thumb:  '-quality 75 -strip',  medium: '-quality 90 -strip' },
    path: "#{Rails.root}/private/attachments/:id/:style/:basename.:extension" ,
    url: "/attachments/:id/:style/:basename.:extension"


  validates_attachment_presence     :data
  validates_attachment_size     :data, :less_than => Proc.new { |asset|
    puts "DATA_CONTENT_TYPE=#{asset.data_content_type.inspect}"
    puts "DATA_FILE_SIZE=#{asset.data_file_size.inspect}"
    case asset.data_content_type
      when 'application/pdf'                                                         then  2000.kilobytes
      when 'audio/mpeg','audio/wav','audio/x-wav'                                    then  5000.kilobytes
      when 'image/png','image/jpg','image/jpeg',"image/gif"                          then  2000.kilobytes
      when 'application/doc','application/docx','application/xlsx','application/xls',
           'application/vnd.ms-office','application/msword',
           'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
           "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"       then  400.kilobytes
      when 'text/plain'                                                              then  200.kilobytes
      when 'application/x-zip','application/zip'                                     then  5000.kilobytes
      else 0
    end
  }
  #validates_attachment_content_type :data, content_type: PAPERCLIP_IMAGE_CONTENT_TYPE

  validates_attachment :data, content_type: { content_type: /\Aimage\/.*\Z/ }

  # Conditional Uniqueness validation on the belongs_to scope
  #validates :default, uniqueness: { scope: :imageable }, if: :default?


  # Methods to set/unset the default image
  # def undefault!
  #   update(default: false)
  # end
  #
  # def default!
  #   imageable.default_image.undefault! if imageable.default_image
  #   update(default: true)
  # end


  def url(*args)
  thumbnailable? ? data.url(*args) : data.url
end

def path
  data.path
end

def name
  data_file_name
end

def content_type
  data_content_type
end

def file_size
  data_file_size
end

def creation_date
  created_at.to_time.strftime("%d de %B de %Y")
end

def thumbnailable?
  ['image/jpeg', 'image/pjpeg', 'image/gif', 'image/png', 'image/x-png', 'image/jpg'].join('').include?(data.content_type)
end

end
