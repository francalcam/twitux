class PostsController < ApplicationController
  before_action :set_post, only: [:show, :edit, :update, :destroy,:user_favorite]

  # GET /posts
  # GET /posts.json
  def index
    #@posts = Post.all
    redirect_to home_path
  end

  # GET /posts/1
  # GET /posts/1.json
  def show
  end

  # GET /posts/new
  def new
    @post = Post.new
  end

  # GET /posts/1/edit
  def edit
  end

  # POST /posts
  # POST /posts.json
  def create
    #raise post_params.inspect
    @post = Post.new(post_params)
    @post.user_id=current_user.id
    @post.parent=Post.find_by_id(post_params[:ancestry]) if post_params[:ancestry]
    respond_to do |format|
      if @post.save
        #format.html { redirect_to @post, notice: 'Post was successfully created.' }
        format.html { redirect_to home_path, notice: 'La publicación se creo correctamente.' }
        format.json { render :show, status: :created, location: @post }
      else
        format.html { render :new }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /posts/1
  # PATCH/PUT /posts/1.json
  def update
    respond_to do |format|
      if @post.update(post_params)
        format.html { redirect_to home_path, notice: 'La publicación se actualizó correctamente.' }
        format.json { render :show, status: :ok, location: @post }
      else
        format.html { render :edit }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /posts/1
  # DELETE /posts/1.json
  def destroy
    @post.destroy
    respond_to do |format|
      format.html { redirect_to home_path, notice: 'La publicación fue borrada.' }
      format.json { head :no_content }
    end
  end

  def comment
    @post=Post.new
    @post.parent = Post.find_by_id(params[:id])
    render :new
  end

  def retwex
    if params[:id]
      original=Post.find_by_id(params[:id])
      cloned_attrs = original.attributes.except("id","created_at","updated_at")
      #DUP MODIFICA EL POST_ID DEL ORIGINAL
      #@post=original.dup
      #CLONE COPIA EL ID, CREAED_AT ...
      #@post=original.clone
      @post=Post.new(cloned_attrs)
      @post.post_id=params[:id]
      @post.user_id=current_user.id
      @post.ancestry=nil
      original.assets.each do |asset|
          cloned_attrs = asset.attributes.except("id","created_at","updated_at")
          a=@post.assets.build(cloned_attrs)
          if asset.data && File.exist?(asset.data.path)
            a.data= asset.data
          end
          raise a.inspect if !a.data
        end
    end
    respond_to do |format|
      if @post.save
        format.html { redirect_to home_path, notice: 'La publicación se Retwexteo correctamente.' }
        format.json { render :show, status: :created, location: @post }
      else
        format.html { render :new }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  def user_favorite
    @post.user_favorite_posts.build(user_id:current_user.id)
    #raise @post.user_favorite_posts.inspect
    respond_to do |format|
      if @post.save
        format.html { redirect_to home_path, notice: 'La publicación se marcó como favorita.' }
        format.json { render :show, status: :created, location: @post }
      else
        format.html { redirect_to home_path, notice: "#{@post.errors.messages}"  }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def post_params
      params.require(:post).permit(:text, :ancestry,
                                    #assets_attributes:[asset:[:id,:data,:description,:tag,:tmp_file_id,:_destroy]]  )
                                    assets_attributes:[:id,:data]  )
    end
end
