class UserSessionsController < ApplicationController
  layout "login"

  def new
    if current_user
      redirect_to home_path
    else
      @user_session = UserSession.new
    end
  end

  def create
    #raise user_session_params.inspect
    @user_session = UserSession.new(user_session_params.to_h)
    #@user_session = UserSession.new(params[:user_session])
    if @user_session.save
      redirect_to home_path
    else
      #raise @user_session.errors.inspect
      render :action => :new
    end
  end

  def destroy
    current_user_session.destroy
    redirect_to new_user_session_url
  end

  private

  def user_session_params
    params.require(:user_session).permit(:login, :email, :password, :remember_me)
  end
end
