class ApplicationController < ActionController::Base
  helper_method :current_user_session, :current_user

  private
    def current_user_session
      return @current_user_session if defined?(@current_user_session)
      @current_user_session = UserSession.find
    end

    def current_user
      return @current_user if defined?(@current_user)
      @current_user = current_user_session && current_user_session.user
    end

    def require_user
      unless current_user
        if request.post?
          @re_post = { data: params, url: request.url }
          @user_session = Ots::UserSession.new
          render 'ots/user_sessions/new', layout: 'ots/user_session'
        else
          store_location
          flash[:notice] = "Debe estar logado para acceder a esta página."
          session[:requested_url] = request.url
          redirect_to new_user_session_url
        end
        return false
      end
    end

    def store_location
      session[:return_to] = request.original_url
    end

end
