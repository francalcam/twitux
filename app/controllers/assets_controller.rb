class AssetsController < ApplicationController
  def show
    attachment = Asset.find(params[:id])
    filename = attachment.data.path
    type = attachment.data.content_type
    send_file filename, :type => type, :x_sendfile=>true
  end
end
