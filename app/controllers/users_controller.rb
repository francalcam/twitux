class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :following, :unfollowing, :destroy]

  #skip_load_and_authorize_resource :only => :new
  #before_action :require_user
  #before_action :authenticate_user!

  # GET /users
  # GET /users.json
  def index
    @users = User.all
  end

  # GET /users/1
  # GET /users/1.json
  def show
  end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        format.html { redirect_to home_path, notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to home_path, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def following
    #raise current_user.inspect
    #@user.followed
    f=UserConnection.new(followed: @user,follower:current_user)
    if f.save
      redirect_to home_path, notice: "Siguiendo a #{@user.login}."
    else
      redirect_to home_path, notice: "ERROR!!!"
    end
    #raise f.errors.inspect unless f.save
  end

  def unfollowing
    #raise current_user.inspect
    #@user.followed
    f=UserConnection.find_by_followed_id(@user)
    if f.destroy
      redirect_to home_path, notice: "Ha dejado de seguir a #{@user.login}."
    else
      redirect_to home_path, notice: "ERROR!!!"
    end
    #raise f.errors.inspect unless f.save
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:id,:login,:name,:last_name,:password,:password_confirmation,asset_attributes:[:id,:data])
    end

end
