class HomeController < ApplicationController
  before_action :require_user
  def index
    @posts = Post.where(ancestry:nil, user_id:current_user.id).or(Post.where(ancestry:nil, user_id:current_user.following))
    @search = Search.new
    if params[:search]
      @users=@search.find(params[:search][:user])
    else
      @users=User.where.not(id:current_user.id)
    end

    @notifications={}
    Post.roots.map(&:children).flatten
    @new_msgs       = Message.where(recipient_id:current_user, check_view:[false,nil])
    @new_followers  = UserConnection.where(followed_id:current_user, check_view:[false,nil])
    @new_posts      = Post.where(user_id:current_user.followers, check_view:[false,nil])
    @new_retwix     = Post.where(post_id:current_user, check_view:[false,nil])
    @new_comments   = Post.where(user_id:current_user, check_view:[false,nil]).map(&:children).flatten
    my_posts = Post.where(user_id:current_user)
    @new_favourites = UserFavoritePost.where(post_id:my_posts)
    # Genérico
    # Rails.application.eager_load!
    # cvs_models=ActiveRecord::Base.descendants.select{|des|!des.abstract_class}.select{|des|des.column_names.include?("check_view")}
    # #ActiveRecord::Base.descendants.select{|des|!des.abstract_class}.select{|des|des.column_names.include?("check_view")}.map(&:name)
    # #["Message", "Post", "PostConnection", "UserConnection"]
    # cvs_models.each do |model|
    #   if model.has_attribute?(:follower_id)
    #     #res=model.where(check_view: [false,nil], follower_id:current_user.id)
    #   elsif model.has_attribute?(:followed_id)
    #     res=model.where(check_view: [false,nil], followed_id:current_user.id)
    #   elsif model.has_attribute?(:user_id)
    #     res=model.where(check_view: [false,nil], user_id:current_user.id)
    #   end
    #   @notifications[model.name]||=res if res.present?
    # end
    #ActiveRecord::Base.descendants.select{|des|!des.abstract_class}.select{|des|des.column_names.include?("check_view")}.map(&:name)

    #session[:user_location] = Location.new(user_ip)
  end
end
