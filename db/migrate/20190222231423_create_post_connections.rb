class CreatePostConnections < ActiveRecord::Migration[5.2]
  def change
    create_table :post_connections do |t|
      #t.references :post_a, foreign_key: true
      #t.references :post_b, foreign_key: true
      t.integer :post_a_id, null: false
      t.integer :post_b_id, null: false
      t.boolean    :check_view

      t.timestamps
    end
  end
end
