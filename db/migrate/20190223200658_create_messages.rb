class CreateMessages < ActiveRecord::Migration[5.2]
  def change
    create_table :messages do |t|
      t.references :user
      t.references :recipient
      t.string :subject, limit: 100
      t.text :message
      t.boolean :read_notification
      t.boolean    :check_view
      t.timestamps
    end
  end
end
