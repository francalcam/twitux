class CreateUserConnections < ActiveRecord::Migration[5.2]
  def change
    create_table :user_connections do |t|
      # t.references :user_a, foreign_key: true
      # t.references :user_b, foreign_key: true
      t.integer :follower_id, null: false
      t.integer :followed_id, null: false
      #t.string :follow_type
      t.boolean    :check_view

      t.timestamps
    end
  end
end
