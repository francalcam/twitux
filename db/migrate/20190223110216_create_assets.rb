class CreateAssets < ActiveRecord::Migration[5.2]
  def change
    #rails g model assets tag:string default:boolean description:string data:attachment attachable:references{polymorphic
    create_table :assets do |t|
      t.string :tag
      t.boolean :default, default: false
      t.string :description
      t.attachment :data
      t.references :attachable, polymorphic: true

      t.timestamps
    end
  end
end
