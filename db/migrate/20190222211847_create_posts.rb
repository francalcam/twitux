class CreatePosts < ActiveRecord::Migration[5.2]
  def change
    create_table :posts do |t|
      t.string :text, limit: 140
      t.string :ancestry, index: true
      t.references :post, index: true#, foreign_key: true
      t.references :user, index: true#, foreign_key: true
      t.boolean :check_view
      t.timestamps
    end
    #add_index :posts, :ancestry
  end
end
