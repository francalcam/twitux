#encoding: UTF-8

class CreateUsers < ActiveRecord::Migration[5.2]
  def up
    create_table :users do |t|
      # Authlogic::ActsAsAuthentic::Email
      t.string    :email
      t.string    :login

      t.string    :name
      t.string    :last_name

      # Authlogic::ActsAsAuthentic::Password
      t.string    :crypted_password
      t.string    :password_salt

      # Authlogic::ActsAsAuthentic::PersistenceToken
      t.string    :persistence_token
      t.index     :persistence_token, unique: true

      # Authlogic::ActsAsAuthentic::SingleAccessToken
      t.string    :single_access_token
      t.index     :single_access_token, unique: true

      # Authlogic::ActsAsAuthentic::PerishableToken
      t.string    :perishable_token
      t.index     :perishable_token, unique: true

      # See "Magic Columns" in Authlogic::Session::Base
      t.integer   :login_count, default: 0, null: false
      t.integer   :failed_login_count, default: 0, null: false
      t.datetime  :last_request_at
      t.datetime  :current_login_at
      t.datetime  :last_login_at
      t.string    :current_login_ip
      t.string    :last_login_ip

      # See "Magic States" in Authlogic::Session::Base
      t.boolean   :active, default: false
      t.boolean   :approved, default: false
      t.boolean   :confirmed, default: false

      t.timestamps
    end
    users=[{login: "fcalahorro", name:"Francisco", last_name:"Calahorro", email:"fcalahorro@thecocktail.com", password:"12345678", password_confirmation: "12345678",active:true,confirmed:true,approved:true},
           {login: "tlopez", name:"Teresa", last_name: "López", email: "tlopez@thecocktail.com", password: "12345678", password_confirmation: "12345678",active:true,confirmed:true,approved:true},
           {login: "fbanares",name:"Fausto", last_name: "Bañares", email: "fbanares@thecocktail.com", password:"12345678", password_confirmation: "12345678",active:true,confirmed:true,approved:true}
          ]
    users.each do |user|
      u=User.new(user)
      u.save!
    end
  end

  def down
    drop_table :users
  end

end
