Rails.application.routes.draw do
  resources :messages
  root :to => "user_sessions#new"
  resources :user_sessions
  get 'home'           => 'home#index'
  post 'home'           => 'home#index'
  get 'login'          => 'user_sessions#new'
  get 'logout'         => 'user_sessions#destroy'
  # post 'authenticate'   => 'user_sessions#create'

  resources :users do
    # collection do
    #   post :following
    # end
    member do
      post :following
      post :unfollowing
    end
  end
  resources :posts do
    member do
      get :comment
      get :retwex
      get :user_favorite
    end
  end
  resources :albums
  #get 'assets/show'
  get 'attachments/:id/:style/:basename.:extension' => "assets#show"
end
