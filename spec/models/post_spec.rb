require 'rails_helper'

RSpec.describe Post, type: :model do
  #pending "add some examples to (or delete) #{__FILE__}"
  describe 'post validates' do
    #it { should ensure_length_of(:text).is_at_most(140) }
    it { is_expected.to validate_length_of(:text).is_at_most(140) }
    it { should validate_presence_of(:text) }
    it { should validate_presence_of(:user) }
  end
end
