#encoding: UTF-8
#require 'spec_helper'
#require 'data_helper'
require 'rails_helper'
require 'users_controller'

#describe UsersController, :type => :controller do
describe UsersController do

  describe "Test Users" do
    describe 'check paths' do
      it 'check path index' do
        get :index
        response.status.should be(200)
        #expect(users_path).to have_http_status(200)
        #expect(users_path).to eq '/users'
      end
    end

    describe 'POST #create' do
      describe 'creates user' do
        it 'with valid attributes' do
          u = User.new( login: "loginnnn",
                        name: "nombre",
                        last_name: "Apellido1 Apellido2",
                        email: "validemail@thecocktail.com",
                        password: "12345678",
                        password_confirmation: "12345678")
          u.valid?
          expect(u).to be_valid
        end
        it 'invalid email (ñ)' do
          u = User.new( login: "login",
                        name: "Ññññ",
                        last_name: "Apellido1 Apellido2",
                        email: "ññññ@thecocktail.com",
                        password: "12345678",
                        password_confirmation: "12345678")
          u.valid?
          #expect(u).not_to be_valid
          expect(u.errors[:email]).to  include("should look like an email address.")
        end
      end
    end



  end
end
