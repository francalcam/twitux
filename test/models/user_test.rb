require 'test_helper'

class UserTest < ActiveSupport::TestCase

  test "User is not created without @ into email" do
    u = User.new(users(:one))
    assert_not u.save
  end
end
